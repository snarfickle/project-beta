import React, {useEffect, useState} from "react";

function TechniciansList () {
    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/'
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setTechnicians(data.technicians)

        }
        
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
    <div>
        <h1 className="mt-3 mb-3 p-0">Technicians List</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <td>Employee ID</td>
                    <td>First Name</td>
                    <td>Last Name</td>
                </tr>
            </thead>
            <tbody>
                {technicians.map(tech => {
                    return (
                        <tr key={tech.employee_id}>
                            <td> {tech.employee_id} </td>
                            <td> {tech.first_name} </td>
                            <td> {tech.last_name} </td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    </div>
    );
}
export default TechniciansList;
