import React, { useState, useEffect, Callback, useCallback } from "react";

function AppointmentListHistory() {
    const [appointments, setAppointmentsList] = useState([]);
    const [filterText, setFilterText] = useState('');


    const fetchData = useCallback(async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json();
            setAppointmentsList(data.appointments);
        }

    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData]);


    function handleFilterChange(e) {
        setFilterText(e.target.value);
    }

    return (

        <div>
            <h1 className="mt-3 mb-3 p-0">Appointments</h1>
            <input
                type="text"
                placeholder="Filter by vin"
                value={filterText}
                onChange={handleFilterChange}
                className="form-control mb-3"
            />
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Reason</th>
                        <th>Vin</th>
                        <th>Technician</th>
                        <th>VIP?</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                
                {appointments
                    .filter(appt => {
                        const vin = appt.vin.vin ? appt.vin.vin.toString() : '';
                        return vin.toLowerCase().includes(filterText.toLowerCase());
                        })
                    .map(appt => {
                        const { vin, sold } = appt.vin;
                        const formattedDate = new Date(appt.date_time).toLocaleDateString();
                        const formattedTime = new Date(appt.date_time).toLocaleTimeString([], { hour: 'numeric', minute: '2-digit', hour12: true });
                        return (
                            <tr key={appt.id}>
                                <td>{appt.customer}</td>                                    
                                <td>{formattedDate}</td>
                                <td>{formattedTime}</td>
                                <td>{appt.reason}</td>
                                <td>{vin}</td>
                                <td>{appt.technician}</td>
                                <td>{sold ? 'VIP' : 'Not VIP'}</td>
                                <td>{appt.status}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default AppointmentListHistory;
