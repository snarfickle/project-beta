import React, { useState, useEffect } from 'react';

function SalespersonList() {
    const [salespersons, setSalespersons] = useState([])

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/salespeople/'
        const response = await fetch(url)

        if(response.ok) {
            const data = await response.json();
            setSalespersons(data.salespersons)
        }
    }

    useEffect(() => {
        fetchData();
    }, [])

    return (
        <div>
            <h1 className="mt-3 mb-3 p-0">Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespersons.map((salesperson) => {
                        return (
                            <tr key={salesperson.id}>
                                <td>{salesperson.employee_id}</td>
                                <td>{salesperson.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default SalespersonList