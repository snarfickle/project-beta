import React, { useState, useEffect } from 'react';

function SaleForm() {
    const[automobiles, setAutomobiles] = useState([])
    const[automobile, setAutomobile] = useState('')
    const handleAutomobileChange = (event) => {
        const value = event.target.value
        setAutomobile(value)
    }
    const[salespersons, setSalespersons] = useState([])
    const[salesperson, setSalesperson] = useState('')
    const handleSalespersonChange = (event) => {
        const value = event.target.value
        setSalesperson(value)
    }
    const[customers, setCustomers] = useState([])
    const[customer, setCustomer] = useState('')
    const handleCustomerChange = (event) => {
        const value = event.target.value
        setCustomer(value)
    }
    const[price, setPrice] = useState('')
    const handlePriceChange = (event) => {
        const value = event.target.value
        setPrice(value)
    }
    const handleSubmit = async (event) => {
        event.preventDefault();

        const sale = {
            automobile: automobile,
            salesperson: salesperson,
            customer: customer,
            price: price,
        }

        const saleUrl = 'http://localhost:8090/api/sales/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(sale),
            headers: {
                'Content-Type': 'application/json',
            },
        }

        const response = await fetch(saleUrl, fetchConfig)
        if(response.ok) {
            const newSale = await response.json()
            console.log(newSale)

            setAutomobile([])
            setSalesperson([])
            setCustomer([])
            setPrice('')
            window.location.replace('/sales')
        } else {
            console.log('error')
        }
    }
    const fetchAutomobileData = async () => {
        const automobileUrl = 'http://localhost:8100/api/automobiles/'

        const response = await fetch(automobileUrl)
        console.log(response)
        if(response.ok) {
            const data = await response.json()
            setAutomobiles(data.autos)
            console.log(data)
        }
    }

    const fetchSalespersonData = async () => {
        const salespersonUrl = 'http://localhost:8090/api/salespeople/'

        const response = await fetch(salespersonUrl)
        console.log(response)
        if(response.ok) {
            const data = await response.json()
            setSalespersons(data.salespersons)
            console.log(data)
        }
    }

    const fetchCustomerData = async () => {
        const customerUrl = 'http://localhost:8090/api/customers/'

        const response = await fetch(customerUrl)
        console.log(response)
        if(response.ok) {
            const data = await response.json()
            setCustomers(data.customers)
            console.log(data)
        }
    }

    useEffect(() => {
        fetchAutomobileData();
    }, [])

    useEffect(() => {
        fetchSalespersonData();
    }, [])

    useEffect(() => {
        fetchCustomerData();
    }, [])

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new salesrecord</h1>
                    <form onSubmit={handleSubmit} id="create-sales-form">
                        <div className="mb-3">
                            <select onChange={handleAutomobileChange} required id="automobile" name="automobile" className="form-select">
                            <option value="">Choose an Automobile VIN</option>
                            {automobiles.map(automobile => {
                                return (
                                    <option key={automobile.vin} value={automobile.vin}>
                                        {automobile.vin}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
                            <option value="">Choose a Salesperson</option>
                            {salespersons.map(salesperson => {
                                return (
                                    <option key={salesperson.id} value={salesperson.id}>
                                        {salesperson.name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} required id="customer" name="customer" className="form-select">
                            <option value="">Choose a Customer</option>
                            {customers.map(customer => {
                                return (
                                    <option key={customer.id} value={customer.id}>
                                        {customer.name}
                                    </option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} placeholder="Price" required type="text" name="price" id="price" className="form-control"/>
                            <label htmlFor="price">Price</label>
                        </div>
                          <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SaleForm;