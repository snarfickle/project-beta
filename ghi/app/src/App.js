import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ManufacturerList from './ManufacturerList';
import ManufacturerForm from './ManufacturerForm';
import VehicleList from './VehicleList';
import SalespersonList from './SalespersonList.js';
import SalespersonForm from './SalespersonForm.js'
import CustomerList from './CustomerList.js';
import CustomerForm from './CustomerForm.js'
import SaleList from './SaleList.js'
import SaleForm from './SaleForm.js'
import SalesHistory from './SalesHistory.js'
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentsList';
import AppointmentListHistory from './AppointmentsListHistory';
import TechnicianForm from './TechnicianForm';
import TechniciansList from './TechniciansList';
import VehicleModelForm from './VehicleForm';
import AutomobileForm from './AutomobileForm';
import AutomobileList from './AutomobilesList';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salespeople" element={<SalespersonList />} />
          <Route path="/salespeople/new" element={<SalespersonForm />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/sales" element={<SaleList />} />
          <Route path="/sales/new" element={<SaleForm />} />
          <Route path="/sales/history" element={<SalesHistory />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/models" element={<VehicleList />} />
          <Route path="/models/new" element={<VehicleModelForm />} />
          <Route path="/appointments/" element={<AppointmentList />} />
          <Route path="/appointments/new" element={<AppointmentForm />} />
          <Route path="/appointments/history" element={<AppointmentListHistory />} />
          <Route path="/technicians" element={<TechniciansList />} />
          <Route path="/technicians/new" element={<TechnicianForm />} />
          <Route path="/automobiles" element={<AutomobileList />} />
          <Route path="/automobiles/add" element={<AutomobileForm />} />

        </Routes>
      </div>
    </BrowserRouter>
  );
}



export default App;
