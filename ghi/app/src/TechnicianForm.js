import { useEffect, useState } from "react";

function TechnicianForm() {
    const [first_name, setFirstName] = useState('');
    const [last_name, setLastName] = useState('');
    const [employee_id, setEmployeeID] = useState(`${first_name.slice(0, 3)}-${last_name}`);


async function handleSubmit(e) {
  e.preventDefault();
  const newEmployeeID=(`${first_name.slice(0, 3)}-${last_name}`);
  setEmployeeID(newEmployeeID);
  const data = {
    first_name,
    last_name,
    employee_id: newEmployeeID,
  };
  const techsURL = 'http://localhost:8080/api/technicians/';

  const fetchConfig = {
    method: "POST",
    body: JSON.stringify(data),
    headers: {
        "Content-Type": "application/json",
    },
  };

  const response = await fetch(techsURL, fetchConfig);


  if (response.ok){
    setFirstName('');
    setLastName('');
    setEmployeeID('');
  };

}

function hanldeFirstNameChange(e) {
  const value = e.target.value;
  setFirstName(value);
}

function handleLastNameChange(e) {
  const value = e.target.value;
  setLastName(value);
}

    return (
      <div className="row">
      <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
              <h1>Create a Technician</h1>
              <form onSubmit={handleSubmit} id="create-technician-form">
                  <div className="form-floating mb-3">
                      <input value={first_name} onChange={hanldeFirstNameChange} placeholder="First Name" required type="text" name="text" id="text" className="form-control" />
                      <label htmlFor="first-name">First Name</label>
                  </div>
                  <div className="form-floating mb-3">
                      <input value={last_name} onChange={handleLastNameChange} placeholder="Last Name" required type="text" name="text" id="text" className="form-control" />
                      <label htmlFor="last-name">Last Name</label>
                  </div>
                  <button className="btn btn-primary">Create</button>
              </form>
          </div>
      </div>
  </div>
 );
}

export default TechnicianForm;
