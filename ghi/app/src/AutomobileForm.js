import React, { useState, useEffect } from "react";

function AutomobileForm () {
    const [color, setColor] = useState('');
    const [year, setYear] = useState('');
    const [vin, setVin] = useState('');
    const [model_id, setModelID] = useState('');
    const [models, setModelsList] = useState([]);

    useEffect(() => { async function fetchModels() {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setModelsList(data.models);
        }
    };
    fetchModels();
    }, []);

    async function handleSubmit(e) {
        e.preventDefault();
        const data = {
            color,
            year,
            vin,
            model_id,
        };
        const modelsURL = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {"Content-Type": "application/json"},
        };

        const response = await fetch(modelsURL, fetchConfig);

        if(response.ok) {
            setColor('');
            setYear('');
            setVin('');
            setModelID('');
        };
    };

    function handleColorChange(e){
        const value = e.target.value;
        setColor(value);
    }
    function handleYearChange(e){
        const value = e.target.value;
        setYear(value);
    }
    function handleVinChange(e){
        const value = e.target.value;
        setVin(value);
    }
    function handleModelChange(e){
        const value = e.target.value;
        setModelID(value);
    }
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add an automobile to inventory</h1>
                <form onSubmit={handleSubmit} id="create-appointment-form">
                    <div className="form-floating mb-3">
                        <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={year} onChange={handleYearChange} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
                        <label htmlFor="year">Year</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={vin} onChange={handleVinChange} placeholder="Vin" required type="text" name="vin" id="vin" className="form-control" />
                        <label htmlFor="vin">Vin</label>
                    </div>
                    <div className="mb-3">
                        <select value={model_id} onChange={handleModelChange} required name="model_id" id="model_id" className="form-select">
                            <option value="model_id">Choose a model</option>
                            {models.map(mod => {
                                return(
                                    <option key={mod.id} value={mod.id}>
                                        {mod.name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    )

}
export default AutomobileForm;
