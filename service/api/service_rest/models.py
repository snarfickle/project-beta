from django.db import models
from django.urls import reverse
    

class Technician(models.Model):
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    employee_id = models.CharField(max_length=150)

    def get_api_url(self):
        return reverse("api_technician_list", kwargs={"pk": self.pk})

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class AutomobileVO (models.Model):
    vin = models.CharField(max_length=150)
    color = models.CharField(max_length=100, null=True)
    year = models.PositiveSmallIntegerField(null=True)
    sold = models.BooleanField(default=False)



class Appointment (models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField(max_length=1000)
    status = models.CharField(max_length=150)
    vin = models.ForeignKey(
        AutomobileVO,
        related_name="appointment",
        on_delete=models.PROTECT,
        default=None,
    ) 
    customer = models.CharField(max_length=200)
    technician = models.ForeignKey(
        Technician, 
        related_name="appointment", 
        on_delete=models.CASCADE,
        )

    def get_api_url(self):
        return reverse("api_appointment_list", kwargs={"pk": self.pk})

    def __str__(self):
        return self.customer
    
