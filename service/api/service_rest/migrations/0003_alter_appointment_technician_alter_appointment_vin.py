# Generated by Django 4.0.3 on 2023-04-25 23:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0002_automobilevo_alter_appointment_status_delete_status_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='appointment',
            name='technician',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='appointment', to='service_rest.technician'),
        ),
        migrations.AlterField(
            model_name='appointment',
            name='vin',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.PROTECT, related_name='appointment', to='service_rest.automobilevo'),
        ),
    ]
