
from django.db import models
from django.urls import reverse

# Create your models here.

class Salesperson(models.Model):
    name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.name}'

class Customer(models.Model):
    name = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=25, unique=True)
    address = models.CharField(max_length=100, null=True)

    def __str__(self):
        return f'{self.name}'

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=50, unique=True)
    color = models.CharField(max_length=100, null=True)
    year = models.PositiveSmallIntegerField(null=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.vin}'

class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobiles",
        on_delete=models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name="salespersons",
        on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer,
        related_name="customers",
        on_delete=models.CASCADE,
    )
    price = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.salesperson} {self.customer} {self.automobile}'
    
